import { Component, OnInit } from '@angular/core';
import { hero } from '../hero';
import { HeroService } from '../hero.service';
// import { HEROES } from '../mock-heroes';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  // hero : hero = {
  //   id:1,
  //   name:"windstorm"
  // }
  // selectedHero : hero;
  // heroes = HEROES;
  heroes : hero[]
  constructor(private heroService : HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  // onSelect(hero : hero) : void
  // {
  //   this.selectedHero = hero;
  // }

  getHeroes() : void{
    // this.heroes = this.heroService.getHeroes();
    this.heroService.getHeroes()
    .subscribe(heroes => this.heroes = heroes)
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  delete(hero: hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero).subscribe();
  }
}
