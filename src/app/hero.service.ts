import { Injectable } from '@angular/core';
import { hero } from './hero';
import { HEROES } from './mock-heroes';
import {Observable, of} from 'rxjs';
import { MessageService } from './message.service'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class HeroService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private messageService : MessageService, private http : HttpClient) { }
  private heroesUrl = 'api/heroes';
  // getHeroes() : hero[]{
  //   return HEROES;
  // }
  // getHeroes() : Observable<hero[]>{
  //   this.messageService.addMessage("Hero Service: Fetched Heroes");
  //   return of(HEROES);
  // }
  getHeroes() : Observable<hero[]>{
    return this.http.get<hero[]>(this.heroesUrl)
    .pipe(tap(_ => this.log('fetched heroes')),catchError(this.handleError<hero[]>("getHeroes",[])))
  }

  // getHero(id : number) : Observable<hero>{
  //   this.messageService.addMessage(`HeroService: fetched hero id=${id}`);
  //   return of(HEROES.find(hero => hero.id === id));
  // } 

  getHero(id: number): Observable<hero> {
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<hero>(url).pipe(
      tap(_ => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<hero>(`getHero id=${id}`))
    );
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.addMessage(`HeroService: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateHero (hero: hero): Observable<any> {
    return this.http.put(this.heroesUrl, hero, this.httpOptions).pipe(
      tap(_ => this.log(`updated hero id=${hero.id}`)),
      catchError(this.handleError<any>('updateHero'))
    );
  }

  addHero (hero: hero): Observable<hero> {
    return this.http.post<hero>(this.heroesUrl, hero, this.httpOptions).pipe(
      tap((newHero: hero) => this.log(`added hero w/ id=${newHero.id}`)),
      catchError(this.handleError<hero>('addHero'))
    );
  }

  deleteHero (hero: hero | number): Observable<hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;
  
    return this.http.delete<hero>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<hero>('deleteHero'))
    );
  }
}
